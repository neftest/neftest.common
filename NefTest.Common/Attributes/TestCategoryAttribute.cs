﻿using NefTest.Common.Enums;
using System;

namespace NefTest.Common.Attributes {
    /// <summary>
    /// Marks a class as a test category. Optionally specify [EnvironmentType](xref:NefTest.Common.Enums.EnvironmentType)
    /// to limit which environments this category will attempt to run under. By default categories will be run under
    /// any available environment.
    /// </summary>
    [AttributeUsage(validOn: AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class TestCategoryAttribute : Attribute {
        /// <summary>
        /// The [EnvironmentType](xref:NefTest.Common.Enums.EnvironmentType) that this category will attempt
        /// to run under.
        /// </summary>
        public EnvironmentType EnvironmentType { get; }

        /// <summary>
        /// Marks a class as being a test category.
        /// </summary>
        /// <param name="environmentType">EnvironmentType this test category will attempt to run under.</param>
        public TestCategoryAttribute(EnvironmentType environmentType = EnvironmentType.Any) {
            EnvironmentType = environmentType;
        }
    }
}
