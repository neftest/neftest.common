﻿using NefTest.Common.Enums;
using System;

namespace NefTest.Common.Attributes {
    /// <summary>
    /// Marks a method as a test case. Optionally specify [EnvironmentType](xref:NefTest.Common.Enums.EnvironmentType)
    /// to limit which environments this test will attempt to run under. By default test cases will be run under
    /// any available environment.
    /// </summary>
    [AttributeUsage(validOn: AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class TestCaseAttribute : Attribute {
        /// <summary>
        /// The [EnvironmentType](xref:NefTest.Common.Enums.EnvironmentType) that this test will attempt
        /// to run under.
        /// </summary>
        public EnvironmentType EnvironmentType { get; }

        /// <summary>
        /// Marks a method as being a test case.
        /// </summary>
        /// <param name="environmentType">EnvironmentType this test will attempt to run under.</param>
        public TestCaseAttribute(EnvironmentType environmentType = EnvironmentType.Any) {
            EnvironmentType = environmentType;
        }
    }
}
