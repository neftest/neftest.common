﻿using System;

namespace NefTest.Common.Enums {
    /// <summary>
    /// Available environment types. An environment is any place a test case can be run from.
    /// 
    /// This is a flag, so it is possible to specify multiple environments.
    /// </summary>
    [Flags]
    public enum EnvironmentType {
        /// <summary>
        /// No environment.
        /// </summary>
        None              = 0x0000,

        /// <summary>
        /// Run in the console test runner.
        /// </summary>
        Console           = 0x0001,

        /// <summary>
        /// Run in an ACClient instance.
        /// </summary>
        ACClient          = 0x1000,

        /// <summary>
        /// Run in a Decal Container instance.
        /// </summary>
        DecalContainer    = 0x2000,

        /// <summary>
        /// Run in any environment.
        /// </summary>
        Any               = 0xFFFF
    }
}
