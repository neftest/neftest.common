﻿using System;

namespace NefTest.Common.Enums {
    /// <summary>
    /// Type of automated test.
    /// </summary>
    [Flags]
    public enum TestType {
        /// <summary>
        /// None.
        /// </summary>
        None = 0x0000,

        /// <summary>
        /// Unit Tests.
        /// </summary>
        Unit = 0x0001,
        
        /// <summary>
        /// End to End Tests.
        /// </summary>
        E2E = 0x0002,

        /// <summary>
        /// All
        /// </summary>
        All = 0xFFFF
    }
}
