﻿using NefTest.Common.Attributes;
using NefTest.Common.Enums;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Common.Tests {
    /// <summary>
    /// TODO: these test are actually being run against NefTest.Common that was loaded by the test runner.
    /// 
    /// Not sure how to fix this currently without resorting to app domains, but that may be required for
    /// at least testing this class since it already is loaded by the runner.
    /// </summary>
    [TestCategory]
    public class TestTestCategoryAttribute {
        public static IEnumerable<AttributeUsageAttribute> GetAttributeUsageAttributes() {
            return (IEnumerable<AttributeUsageAttribute>)typeof(TestCategoryAttribute).GetCustomAttributes(typeof(AttributeUsageAttribute), false);
        }

        [TestCase]
        public void Has_Single_AttributeUsage() {
            var attributes = GetAttributeUsageAttributes();

            attributes.Count().ShouldBe(1);
        }

        [TestCase]
        public void Does_Not_Allow_Multiple_Attribute_Definitions() {
            var attribute = GetAttributeUsageAttributes().First();

            attribute.AllowMultiple.ShouldBeFalse();
        }

        [TestCase]
        public void Attribute_Is_Inheritable() {
            var attribute = GetAttributeUsageAttributes().First();

            attribute.Inherited.ShouldBeTrue();
        }

        [TestCase]
        public void Default_Environment_Is_Any() {
            var testCaseAttribute = new TestCategoryAttribute();

            testCaseAttribute.EnvironmentType.ShouldBe(EnvironmentType.Any);
        }

        [TestCase]
        public void Attribute_Constructor_Sets_Properties() {
            var testCaseAttribute = new TestCategoryAttribute(EnvironmentType.ACClient);

            testCaseAttribute.EnvironmentType.ShouldBe(EnvironmentType.ACClient);
        }
    }
}
