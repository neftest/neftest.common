﻿using NefTest.Common.Attributes;
using NefTest.Common.Enums;
using Shouldly;
using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace NefTest.Common.Tests {
    /// <summary>
    /// TODO: these test are actually being run against NefTest.Common that was loaded by the test runner.
    /// 
    /// Not sure how to fix this currently without resorting to app domains, but that may be required for
    /// at least testing this class since it already is loaded by the runner.
    /// </summary>
    [TestCategory]
    public class TestEnvironmentType {
        [TestCase]
        public void Any_Environment_Matches_Every_Environment() {
            var allEnvironmentsExceptAny = Enum.GetValues(typeof(EnvironmentType))
                .Cast<EnvironmentType>()
                .ToList()
                .Where(e => e != EnvironmentType.Any);

            foreach (var env in allEnvironmentsExceptAny) {
                (EnvironmentType.Any & env).ShouldBe(env);
            }
        }

        [TestCase]
        public void None_Environment_Matches_No_Environments() {
            var allEnvironmentsExceptNone = Enum.GetValues(typeof(EnvironmentType))
                .Cast<EnvironmentType>()
                .ToList()
                .Where(e => e != EnvironmentType.None);

            foreach (var env in allEnvironmentsExceptNone) {
                (EnvironmentType.None & env).ShouldNotBe(env);
            }
        }
    }
}
