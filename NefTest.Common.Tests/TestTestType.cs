﻿using NefTest.Common.Attributes;
using NefTest.Common.Enums;
using Shouldly;
using System;
using System.Linq;

namespace NefTest.Common.Tests {
    /// <summary>
    /// TODO: these test are actually being run against NefTest.Common that was loaded by the test runner.
    /// 
    /// Not sure how to fix this currently without resorting to app domains, but that may be required for
    /// at least testing this class since it already is loaded by the runner.
    /// </summary>
    [TestCategory]
    public class TestTestType {
        [TestCase]
        public void All_TestType_Matches_Every_TestType() {
            var allTestTypesExceptAll = Enum.GetValues(typeof(TestType))
                .Cast<TestType>()
                .ToList()
                .Where(e => e != TestType.All);

            foreach (var env in allTestTypesExceptAll) {
                (TestType.All & env).ShouldBe(env);
            }
        }

        [TestCase]
        public void None_TestType_Matches_No_TestTypes() {
            var allTestTypesExceptNone = Enum.GetValues(typeof(TestType))
                .Cast<TestType>()
                .ToList()
                .Where(e => e != TestType.None);

            foreach (var env in allTestTypesExceptNone) {
                (TestType.None & env).ShouldNotBe(env);
            }
        }
    }
}
